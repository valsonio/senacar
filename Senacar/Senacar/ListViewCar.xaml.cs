﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Senacar
{
    public class Servico
    {
        public string Nome_Modelo { get; set; }
        public float Preco_Modelo { get; set; }

        public string Preco_Formatado
        {
            get { return string.Format("R$ {0}", Preco_Modelo); }
        }

        public string Nome_Marca { get; set; }

    }
    public partial class ListViewCar : ContentPage
    {
        public List<Servico> Veiculos { get; set; }

        public ListViewCar()
        {
            InitializeComponent();

            this.Veiculos = new List<Servico>
            {
                new Servico { Nome_Marca = "Volkswagen", Nome_Modelo = "Gol", Preco_Modelo = 47000},
                new Servico { Nome_Marca = "Fiat", Nome_Modelo = "Argo", Preco_Modelo = 62000 },
                new Servico { Nome_Marca = "Chevrolet", Nome_Modelo = "Cruze", Preco_Modelo = 93000  },
                new Servico { Nome_Marca = "Ford", Nome_Modelo = "Fiesta", Preco_Modelo =  51000},
                new Servico { Nome_Marca = "Hyundai", Nome_Modelo = "HB20", Preco_Modelo = 47000}
            };
            this.BindingContext = this;
                  
        }

   
        private void ListViewSenacar_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var servico = (Servico)e.Item;
            Navigation.PushAsync(new DescricaoView(servico));
        }
    }

}

