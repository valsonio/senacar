﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacar
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DescricaoView : ContentPage
	{


        private const float VIDROS_ELE = 545;
        private const float TRAVAS_ELE = 260;
        private const float AR_CONDICIONADO = 480;
        private const float CAMERA_RÉ = 180;
        private const float CAMBIO = 460;
        private const float SUSPENSAO = 380;
        private const float FREIOS = 245;

        public string TextoVidro
        {
            get
            {
                return string
                    .Format("VidrosEletricos - R$ {0}", VIDROS_ELE);
            }
        }
        public string TextoTravas
        {
            get
            {
                return string
                    .Format("TravasEletricas - R$ {0}", TRAVAS_ELE);
            }
        }
        public string TextoArcondicionado
        {
            get
            {
                return string
                    .Format("ArCondicionado - R$ {0}", AR_CONDICIONADO);
            }
        }
        public string TextoCamera
        {
            get
            {
                return string
                    .Format("CameraRé - R$ {0}", CAMERA_RÉ);
            }
        }
        public string TextoCambio
        {
            get
            {
                return string
                    .Format("Câmbio - R$ {0}", CAMBIO);
            }
        }
        public string TextoSuspensao
        {
            get
            {
                return string
                    .Format("Suspensão - R$ {0}", SUSPENSAO);
            }
        }
        public string TextoFreios
        {
            get
            {
                return string
                    .Format("Freios - R$ {0}", FREIOS);
            }
        }

        public string ValorTotal
        {
            get
            {
                return string.Format("Valor total: R$ {0}", Servico.Preco_Modelo
                    + (IncluiVidrosEletricos ? VIDROS_ELE : 0)
                    + (IncluiTravasEletricas ? TRAVAS_ELE : 0)
                    + (IncluiArCondicionado ? AR_CONDICIONADO : 0)
                    + (IncluiCameraDeRé ? CAMERA_RÉ : 0)
                    + (IncluiCâmbio ? CAMBIO : 0)
                    + (IncluiSuspensão ? SUSPENSAO : 0)
                    + (IncluiFreios ? FREIOS : 0)
                    );

            }
        }

        bool incluiVidrosEletricos;
        public bool IncluiVidrosEletricos
        {
            get
            {
                return incluiVidrosEletricos;
            }

            set
            {
                incluiVidrosEletricos = value;
                OnPropertyChanged(nameof(ValorTotal));

            }
        }

        bool incluiTravasEletricas;
        public bool IncluiTravasEletricas
        {
            get
            {
                return incluiTravasEletricas;
            }

            set
            {
                incluiTravasEletricas = value;
                OnPropertyChanged(nameof(ValorTotal));

            }
        }

        bool incluiArCondicionado;
        public bool IncluiArCondicionado
        {
            get
            {
                return incluiArCondicionado;
            }

            set
            {
                incluiArCondicionado = value;
                OnPropertyChanged(nameof(ValorTotal));

            }
        }

        bool incluiCameraDeRé;
        public bool IncluiCameraDeRé
        {
            get
            {
                return incluiCameraDeRé;
            }

            set
            {
                incluiCameraDeRé = value;
                OnPropertyChanged(nameof(ValorTotal));

            }
        }

        bool incluiCâmbio;
        public bool IncluiCâmbio
        {
            get
            {
                return incluiCâmbio;
            }

            set
            {
                incluiCâmbio = value;
                OnPropertyChanged(nameof(ValorTotal));

            }
        }

        bool incluiSuspensão;
        public bool IncluiSuspensão
        {
            get
            {
                return incluiSuspensão;
            }

            set
            {
                incluiSuspensão = value;
                OnPropertyChanged(nameof(ValorTotal));

            }
        }

        bool incluiFreios;
        public bool IncluiFreios
        {
            get
            {
                return incluiFreios;
            }

            set
            {
                incluiFreios = value;
                OnPropertyChanged(nameof(ValorTotal));

            }
        }

        public Servico Servico { get; set; }

        public DescricaoView (Servico servico)
		{
			InitializeComponent ();

            this.Title = servico.Nome_Modelo;
            this.Servico = servico;
            this.BindingContext = this;
		}

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgendamentoView(Servico));
        }
    }
}